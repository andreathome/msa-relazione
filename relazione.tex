\documentclass[12pt, a4paper, notitlepage]{article}

\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{pgfplots}
\usepackage{url}

\title{Progetto di Metodi statistici per l'apprendimento}
\author{Sumeersingh Andrea Seegoolam}

\floatname{algorithm}{Algoritmo}

\begin{document}
	\maketitle

\section{Introduzione}
\label{sec:intro}
Questo progetto tratta l'implementazione di un algoritmo di apprendimento di predittori ad albero.

Gli algoritmi di riferimento sono ID3 e C4.5 di J. R. Quinlan.

Il progetto è stato sviluppato in Python 3.7. La libreria sklearn consente di utilizzare una implementazione di CART (variante di C4.5), usata come riferimento qualitativo e prestazionale dei risultati di questo progetto.

L'algoritmo di base è stato poi modificato per poter creare alberi \textit{pruned} e \textit{random forest} con l'obiettivo di ottenere prestazioni migliori.

Il report è strutturato nel seguente modo. La sezione \ref{sec:trees} introduce l'algoritmo di apprendimento e l'algoritmo di pruning di alberi di decisione. La sezione \ref{sec:forest} introduce le tecniche di bagging e random subspace per apprendere random forest. La sezione \ref{sec:data} descrive il dataset di riferimento. Infine la sezione \ref{sec:exp} descrive la sperimentazione eseguita e mostra i risultati ottenuti.

\newpage
\section{Alberi di decisione}
\label{sec:trees}

Questa sezione descrive la struttura generale di un albero di decisione e gli algoritmi usati per la sua costruzione e per la predizione di dati nuovi. Infine è descritto l'algoritmo di costruzione di un albero pruned.

\subsection{Struttura e Predizione}
Un albero di decisione è rappresentato come una struttura dati ad albero ordinato con radice.
Ogni nodo interno dell'albero può avere un numero arbitrario di nodi figli ma essi hanno un ordine stabilito.
Ad ogni nodo interno è associato un \textit{test} della forma $ F_i(x) \leq t $, dove $ F_i(x) $ è il valore della \textit{i}-esima feature dell'esempio \textit{x} e \textit{t} è un valore soglia. In questo caso i test possono indicare esito negativo o positivo ma in generale possono essere ammesse più alternative. In questo progetto il numero di figli possibili è stato quindi limitato a due, ottenendo alberi di decisione binari.
Ad ogni nodo foglia è associata un'etichetta o classe di dato.
Un particolare nodo è indicato come nodo radice dell'albero.
La figura \ref{fig:example} mostra la struttura di un albero di decisione in cui gli archi uscenti dai nodi interni hanno etichettato l'esito del test associato al nodo.

\begin{center}
\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth, keepaspectratio]{example}
	\caption{Esempio di albero di decisione}
	\label{fig:example}
\end{figure}
\end{center}

\newpage
La predizione della classe di un nuovo esempio \textit{x} avviene secondo l'algoritmo mostrato di seguito che richiede la radice \textit{ROOT} dell'albero di decisione.

\begin{algorithm}[H]
	\caption{Predizione con albero di decisione}
	\begin{algorithmic}[1]
		\STATE current\_node $\gets$ ROOT
		\WHILE{ current\_node is not a leaf }
		\IF{ current\_node.test(x) == true }
		\STATE current\_node = current\_node.left\_subtree
		\ELSE
		\STATE current\_node = current\_node.right\_subtree
		\ENDIF
		\ENDWHILE
		\RETURN current\_node.class
	\end{algorithmic}
\end{algorithm}

L'algoritmo applica una visita dell'albero di decisione, partendo dal nodo \textit{ROOT}, seguendo il cammino indotto dai test dei nodi interni sul dato \textit{x}. Quando viene raggiunta una foglia dell'albero, viene ritornata l'etichetta associata alla foglia.

\subsection{Costruzione}
 L'algoritmo di costruzione implementato è basato sull'algoritmo ID3 \cite{quinlan1986induction} e sulla sua evoluzione C4.5 \cite{quinlan2014c4}.
 La costruzione di un albero di decisione è un processo top-down che inizia dalla radice dell'albero, a cui vengono associati tutti gli esempi del training set. 
 Dopodiché si ricerca il test sulle feature che meglio divide gli esempi, sfruttando i concetti di \textit{entropy} e \textit{information gain}, spiegati nelle sezioni successive.
 Per ogni possibile output del test si crea un nodo figlio e gli esempi associati al nodo padre vengono ripartiti nei figli in base all'output dato dal test. 
 Se un nodo ha associati solo esempi appartenenti ad una stessa classe, allora viene direttamente convertito in nodo foglia etichettato con la medesima classe.
 La costruzione dell'albero prosegue fino a che tutti gli esempi vengono associati ad un nodo foglia etichettato con la propria classe.
 Questo tipo di costruzione crea quindi un albero completo per i dati del training set, cioè con training error pari a zero, e che può quindi soffrire di overfitting.
 
 \subsubsection{Entropy e Information gain}
 Entropy e Information gain sono le due metriche utilizzate per determinare il miglior test applicabile ad un nodo interno.

 L'entropy è una misura della quantità di informazione nei dati. Dato un nodo \textit{x} l'entropy si calcola con la seguente formula: 
 $$ E(x) = - \sum_{i=1}^{C} P(c_i) \times log_2 P(c_i) $$
 dove  \textit{C} è il numero di classi e \textit{P($c_i$)} è la probabilità di scegliere un esempio etichettato con la classe \textit{$c_i$} tra gli esempi di \textit{x}. Nel caso \textit{E = 0} tutti gli esempi appartengono alla stessa classe, mentre con \textit{$ E > 0 $} sono presenti almeno due classi diverse.
 
 L'information gain misura la variazione di informazione in seguito a una suddivisione dei dati in più nodi.
 La formula seguente misura l'information gain tra il nodo iniziale \textit{x} e gli \textit{n} nodi figli \textit{$y_i$}.
 $$ IG(x, y) = \sum_{i=1}^n ( E(y_i) ) - E(x) $$
 
Ad ogni nodo interno viene associato il test che, a seguito della suddivisione degli esempi nei nodi figli, comporta l'information gain massimo.
 
\subsection{Pruning}
Per contrastare l'overfitting di un albero di decisione completo si fa uso della tecnica di \textit{pruning}. Questa tecnica prevede di limitare i nodi dell'albero in modo da ridurne la complessità  eliminando dettagli che danno poca informazione. La tecnica di pruning implementata è la \textit{reduced error pruning} \cite{quinlan1987simplifying} in cui si applica l'algoritmo descritto in seguito a tutti i nodi interni partendo dai nodi più distanti e risalendo fino alla radice, usando un validation set di dati.

Si sostituisce un nodo interno \textit{n} con un nodo foglia etichettato con la classe più frequente nel sotto-albero di radice \textit{n} (eliminando tutto il sotto-albero). Si verifica il validation error del nuovo albero. Se questo non aumenta, la modifica viene mantenuta.
Si prosegue fino a che tutti i nodi interni sono stati processati.
Il risultato è quindi un albero \textit{pruned}, cioè con meno nodi rispetto all'albero originale ma con potenza predittiva maggiore.


\newpage
\section{Random Forest}
\label{sec:forest}

Il meta-algoritmo Random Forest \cite{breiman2001random} incorpora l'algoritmo di costruzione base di alberi di decisione applicando due tecniche aggiuntive: \textit{Bootstrap Aggregating} e \textit{Random Subspace}.

\subsection{Bootstrap Aggregating (Bagging)}
Il meta-algoritmo Bootstrap aggregating \cite{breiman1996bagging}, detto anche bagging, è una tecnica ensemble per classificare sfruttando più predittori base.

L'algoritmo è diviso in due fasi. 
Nella prima fase si generano \textit{n} nuovi training set a partire da quello originale. Il processo di creazione dei dataset è semplicemente un'estrazione con ripetizione dei dati dal training set originale usando una distribuzione uniforme.
Nella seconda fase viene eseguito un algoritmo di apprendimento di base su ciascun training set generato precedentemente ottenendo così \textit{n} diversi predittori.

In fase di predizione dei dati, si eseguono tutti i predittori dell'ensemble e si predice con la classe risultante più frequente.

Questa tecnica permette di creare alberi di decisione indipendenti con una bassa correlazione reciproca. Questa caratteristica permette di ottenere foreste con una migliore potenza predittiva, derivante dalla maggior robustezza dell'ensemble rispetto ai singoli alberi in base al rumore dei dati. Al contrario, una foresta con una alta correlazione tra i singoli alberi porta ad una potenza predittiva simile a quella di un singolo albero, che è quindi più sensibile agli errori nei dati.

\subsection{Random Subspace}
Per creare predittori random forest si fa uso della tecnica di Random Subspace \cite{ho1995random}. In essa si sfrutta l'algoritmo di costruzione base di alberi di decisione.
Nella fase di scelta del miglior test per un nodo interno, invece di considerare tutte le feature dei dati, si considera solo un sottoinsieme random di feature.
Questo viene applicato per ogni albero della foresta in modo indipendente.

Questa tecnica permette di ridurre ulteriormente la correlazione tra i singoli alberi di decisione della foresta e quindi di migliorare la qualità delle predizioni.

\newpage
\section{Dataset}
\label{sec:data}
Il dataset utilizzato nel progetto ha come esempi dei giocatori di un videogioco di calcio. Le feature rappresentano caratteristiche inerenti il gioco mentre le etichette indicano il tipo di giocatore e la posizione in campo dello stesso. Il dataset è stato estratto da Kaggle \cite{fifa} e comprende circa 17000 esempi.

Le feature sono state divise in due gruppi: feature base e feature estese. Le feature base indicano caratteristiche generali del giocatore e sono utilizzate per determinare il tipo di giocatore (4 tipi diversi). Le feature estese indicano caratteristiche più dettagliate e sono utilizzate per determinare la posizione in campo del giocatore (15 posizioni diverse).

Le feature di base sono: altezza, peso, velocità, dribbling, tiro, passaggio, difesa, fisicità, piede preferito. Altezza e peso sono rappresentati da valori interi espressi rispettivamente in centimetri e chilogrammi. Il piede preferito è rappresentato da valori zero o uno. Le altre feature sono rappresentate da valori interi nell'intervallo (0, 99).

Le classi che indicano il tipo di giocatore sono: portiere, difesa, centrocampo, attacco.
Nota: i portieri non hanno valori per velocità, dribbling, tiro, passaggio, difesa e fisicità. Sono stati quindi settati a 0.

Le feature estese sono: accelerazione, velocità massima, agilità, equilibrio, reazione, controllo palla, dribbling, compostezza, posizionamento, finalizzazione, potenza tiro, tiri dalla distanza, tiri al volo, rigori, visione, cross, calci di punizione, passaggi corti, passaggi lunghi, curva di tiro, intercetti, testa, marcatura, tackle, scivolata, salto, resistenza, forza, aggressività. Tutte queste feature sono rappresentate da valori interi nell'intervallo (0, 99).

Le classi che indica la posizione in campo di un giocatore sono: portiere, difensore centrale, difensore terzino sx, difensore terzino dx, centrocampista mediano, centrocampista centrale, centrocampista avanzato, centrocampista esterno sx, centrocampista esterno dx, attaccante centrale, attaccante punta, attaccante ala sx, attaccante ala dx.

\newpage
\section{Sperimentazione}
\label{sec:exp}
La sperimentazione consiste nell'esecuzione degli algortmi di apprendimento descritti nelle sezioni precedenti sul dataset di riferimento e la raccolta dei risultati in termini di qualità predittive (training e test error) e tempo di calcolo richiesto.

Gli algoritmi testati sono i seguenti: algoritmo base (1), algoritmo base con pruning (2), random forest (3) e l'algoritmo di riferimento della libreria sklearn di scikit-learn (4).

La sperimentazione è stata eseguita in due fasi. Nella prima fase sono state utilizzate le feature di base e i tipi di giocatore come etichette. Nella seconda fase sono state aggiunte le feature estese con la posizione dei giocatori come etichette.
Per ciascuna fase sono stati utilizzati training set, estratti in modo casuale dal dataset originale, di dimensione crescente (1\% rappresentato in nero, 5\% in rosso e 10\% in blu). Gli esempi restanti del dataset sono stati utilizzati come test set. Per l'algoritmo random forest è stato scelto di creare dieci alberi nell'ensemble in ciascuna fase.

\subsection{Feature base}

I seguenti istogrammi indicano i risultati in termini di training e test error dei quattro algoritmi rispetto agli stessi training e test set. Gli algoritmi sono stati eseguiti dieci volte per ogni configurazione e la media degli errori è presente negli istogrammi.

\begin{center}
\begin{tikzpicture}
\begin{axis}[title=Training set, grid=major, xlabel=algoritmo, ylabel=training error \%, xtick={1,2,3,4}, ymin=0, ymax=5 ]
\addplot[ybar, fill=black]	coordinates {(1,0)(2,6)(3,1.2)(4, 0)};
\addplot[ybar, fill=red]  coordinates {(1.3,0)(2.3,1.8)(3.3,0.8)(4.3, 0)};
\addplot[ybar, fill=blue] coordinates {(1.6,0)(2.6,2.2)(3.6,1)(4.6, 0)};
\end{axis}
\end{tikzpicture}
\end{center}

Sul training set gli algoritmi base (1 e 4) hanno training error dello 0\% per costruzione. L'albero pruned (2) ed il random forest (3) hanno training error più alti derivanti dalle astrazioni indotte dal pruning e dall'ensemble.

\begin{center}
\begin{tikzpicture}
\begin{axis}[title=Test set, grid=major, xlabel=algoritmo, ylabel=test error \%, xtick={1,2,3,4}, ymin=0, ymax=30 ]
\addplot[ybar, fill=black] coordinates {(1,26.1)(2,23.5)(3,22.6)(4,26.1)};
\addplot[ybar, fill=red] coordinates {(1.3,22.2)(2.3,19.2)(3.3,18.2)(4.3,22.6)};
\addplot[ybar, fill=blue] coordinates {(1.6,21.8)(2.6,18.8)(3.6,17.1)(4.6,21.7)};
\end{axis}
\end{tikzpicture}
\end{center}

Sul test set tutti gli algoritmi hanno test error tra 18\% e 25\%. Aumentando la dimensione del training set si ottengono predittori migliori. In questo caso il random forest ottiene i risultati migliori con un test error vicino al 15\%.

Il seguente istogramma mostra i tempi di calcolo minimi richiesti dai quattro algoritmi per le rispettive esecuzioni.

\begin{center}
\begin{tikzpicture}
\begin{axis}[title=Tempi di calcolo, grid=major, xlabel=algoritmo, ylabel=secondi, xtick={1,2,3,4}, ytick={0,1,...,10}, ymin=0, ymax=10 ]
\addplot[ybar, fill=black] coordinates {(1,0.10)(2,0.26)(3,0.31)(4,0.00)};
\addplot[ybar, fill=red] coordinates {(1.3,0.83)(2.3,1.73)(3.3,2.94)(4.3,0.02)};
\addplot[ybar, fill=blue] coordinates {(1.6,2.21)(2.6,4.51)(3.6,7.20)(4.6,0.03)};
\end{axis}
\end{tikzpicture}
\end{center}

\subsection{Feature estese}

Di seguito sono mostrati i risultati della sperimentazione utilizzando le feature estese e le posizioni in campo come etichette.


\begin{center}
\begin{tikzpicture}
\begin{axis}[title=Training set, grid=major, xlabel=algoritmo, ylabel=training error \%, xtick={1,2,3,4}, ymin=0, ymax=30 ]
\addplot[ybar, fill=black] coordinates {(1,0)(2,33.1)(3,0)(4, 0)};
\addplot[ybar, fill=red] coordinates {(1.3,0)(2.3,24.1)(3.3,0.8)(4.3, 0)};
\addplot[ybar, fill=blue] coordinates {(1.6,0)(2.6,26)(3.6,0.05)(4.6, 0)};
\end{axis}
\end{tikzpicture}
\end{center}

Sul training set si ottengono risultati simili al precedente esperimento. In questo caso l'albero pruned (2) ottiene training error elevati mentre per il random forest è quasi nullo. La motivazione dell'elevato training error dell'algoritmo (2) è attribuibile al fatto che in questo caso il pruning dell'albero di base elimini molti più sotto-alberi rispetto al precedente perchè l'algoritmo di base (1) crea alberi più grandi e complessi.

\begin{center}
\begin{tikzpicture}
\begin{axis}[title=Test set, grid=major, xlabel=algoritmo, ylabel=test error \%, xtick={1,2,3,4}, ymin=0, ymax=60, ytick={0,10,...,60} ]
\addplot[ybar, fill=black] coordinates {(1,49.6)(2,53.7)(3,53.5)(4,48.8)};
\addplot[ybar, fill=red] coordinates {(1.3,46.2)(2.3,40.5)(3.3,39.5)(4.3,45.6)};
\addplot[ybar, fill=blue] coordinates {(1.6,43.4)(2.6,38.9)(3.6,36)(4.6,43.3)};
\end{axis}
\end{tikzpicture}
\end{center}

I test error sono più alti rispetto al precedente esperimento. Questo è principalmente dovuto al maggior numero di classi che richiede training set più grandi da dare agli algoritmi per ottenere risultati simili rispetto al precedente.

\begin{center}
	\begin{tikzpicture}
	\begin{axis}[title=Tempi di calcolo, grid=major, xlabel=algoritmo, ylabel=secondi, xtick={1,2,3,4}, ytick={0,5,...,30}, ymin=0, ymax=30 ]
	\addplot[ybar, fill=black] coordinates {(1,0.7)(2,1.1)(3,0.5)(4,0.05)};
	\addplot[ybar, fill=red] coordinates {(1.3,6.6)(2.3,8.8)(3.3,4.8)(4.3,0.05)};
	\addplot[ybar, fill=blue] coordinates {(1.6,15.8)(2.6,25.7)(3.6,12.6)(4.6,0.1)};
	\end{axis}
	\end{tikzpicture}
\end{center}

\newpage
\bibliographystyle{plain}
\bibliography{rel}

\newpage
\appendix
\section{Sperimentazione su dataset Iris}
Per ampliare la sperimentazione, gli algoritmi sono stati testati anche sul dataset Iris \cite{iris}. Il dataset Iris è uno dei dataset più utilizzati nel contesto di algoritmi di machine learning.
Esso è formato da 150 esempi composti da 4 feature e 3 classi.

Gli algoritmi sono stati eseguiti 5 volte usando training set di dimensioni pari al 50\% del dataset. Gli istogrammi seguenti mostrano training e test error minimi (in blu) e medi (in nero) per tutte le esecuzioni.


\begin{center}
	\begin{tikzpicture}
	\begin{axis}[title=Training set, grid=major, xlabel=algoritmo, ylabel=training error \%, xtick={1,2,3,4}, ymin=0, ymax=5 ]
	\addplot[ybar, fill=black] coordinates {(1,0)(2,1.6)(3,0.1)(4, 0)};
	\end{axis}
	\end{tikzpicture}
\end{center}

\begin{center}
	\begin{tikzpicture}
	\begin{axis}[title=Test set, grid=major, xlabel=algoritmo, ylabel=test error \%, xtick={1,2,3,4}, ymin=0, ymax=10 ]
	\addplot[ybar, fill=black] coordinates {(1,6)(2,6.7)(3,5.1)(4,6)};
	\addplot[ybar, fill=blue] coordinates {(1,2.6)(2,2.7)(3,1.3)(4,2.6)};
	\end{axis}
	\end{tikzpicture}
\end{center}


\end{document}